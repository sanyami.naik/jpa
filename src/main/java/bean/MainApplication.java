package bean;

import bean.Teacher;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


public class MainApplication {
    //public static void main(String[] args) {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("teacherDetails");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder= entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery1=criteriaBuilder.createQuery();
        Root<Teacher> from1=criteriaQuery1.from(Teacher.class);

        System.out.println("Select all records from Animals");
        CriteriaQuery<Object> select1=criteriaQuery1.select(from1);
        TypedQuery<Object> typedQuery=entityManager.createQuery(select1);
        List<Object> resultlist=typedQuery.getResultList();
        resultlist.stream().forEach(System.out::println);

        entityManager.getTransaction().begin();

//        Teacher teacher1=new Teacher();
//        teacher1.setTname("Sanyami");
//        teacher1.setTid(1);
//        teacher1.setSalary(23000.80);
//        teacher1.setDeg("junior");
//
//        Teacher teacher2=new Teacher();
//        teacher2.setTname("Ayushman");
//        teacher2.setTid(2);
//        teacher2.setSalary(34089.80);
//        teacher2.setDeg("senior");
//
//        Teacher teacher3=new Teacher();
//        teacher3.setTname("Praveer");
//        teacher3.setTid(3);
//        teacher3.setSalary(45089.80);
//        teacher3.setDeg("senior");
//
//        entityManager.persist(teacher1);
//        entityManager.persist(teacher2);
//        entityManager.persist(teacher3);
//
//
//        entityManager.getTransaction().commit();



        System.out.println("Before Updation");
        Teacher teacherById=entityManager.find(Teacher.class,1);
        System.out.println(teacherById);

        System.out.println("After Updation");
        teacherById.setSalary(1000000);
        System.out.println(teacherById);


        //Deleting by id
        Teacher teacherDeleteById=entityManager.find(Teacher.class,3);
        entityManager.remove(teacherDeleteById);


        entityManager.getTransaction().commit();
        entityManagerFactory.close();
        entityManager.close();
    }
}
